import React, { useState } from 'react';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import TaskCard from '../../components/TaskCard';
import CustomButton from '../../components/CustomButton';
import { ToolBar, Contents, LocationBox } from './BrowsePage.style';

const BrowsePage = () => {
  const [type, setType] = useState(1);
  const [sortType, setSortType] = useState(1);
  const [suburb, setSuburb] = useState('');
  const [distance, setDistance] = useState('');
  const [locationBox, setLocationBox] = useState(false);
  const [postcode, setPostcode] = useState('');
  const [state, setState] = useState('');
  const [isError, setIsError] = useState(false);
  const [location, setLocation] = useState('Set location coditions');

  const toggleLocationBox = () => {
    setIsError(false);
    if (suburb + postcode + state + distance === '') {
      setLocation('Set location coditions');
      setLocationBox(!locationBox);
    } else if (suburb && postcode && state && distance) {
      setLocation([suburb, postcode, state, distance + ' km'].join(', '));
      setLocationBox(!locationBox);
    } else {
      setIsError(true);
    }
  };

  const clearConditions = () => {
    setLocation('Set location coditions');
    setPostcode('');
    setState('');
    setSuburb('');
    setDistance('');
    setIsError(false);
  };

  const handleTypeChange = (event) => {
    setType(event.target.value);
  };

  const handleSortTypeChange = (event) => {
    setSortType(event.target.value);
  };

  return (
    <div>
      <ToolBar>
        <Paper component="form" sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 300 }}>
          <InputBase
            sx={{ ml: 1, flex: 1 }}
            placeholder="search for a task"
            inputProps={{ 'aria-label': 'search for a task' }}
          />
          <IconButton type="submit" sx={{ p: '13px' }} aria-label="search">
            <SearchIcon />
          </IconButton>
        </Paper>
        <CustomButton onClick={toggleLocationBox} color="black">
          {location}
        </CustomButton>
        {locationBox ? (
          <LocationBox>
            <TextField
              id="outlined-basic"
              name="postcode"
              value={postcode}
              label="postcode"
              size="small"
              sx={{ width: '150px' }}
              onChange={(e) => setPostcode(e.target.value)}
              error={isError}
            />
            <TextField
              id="outlined-basic"
              name="suburb"
              value={suburb}
              label="suburb"
              size="small"
              sx={{ width: '150px' }}
              onChange={(e) => setSuburb(e.target.value)}
              error={isError}
            />
            <TextField
              id="outlined-basic"
              name="state"
              value={state}
              label="state"
              size="small"
              sx={{ width: '150px' }}
              onChange={(e) => setState(e.target.value)}
              error={isError}
            />
            <div className="distanceBox">
              <TextField
                id="outlined-basic"
                name="ditance"
                value={distance}
                label="distance"
                size="small"
                type="number"
                sx={{ width: '100px' }}
                onChange={(e) => setDistance(e.target.value)}
                error={isError}
              />
              <label>km</label>
            </div>
            <CustomButton color="primary" variant="outlined" size="small" onClick={clearConditions}>
              clear
            </CustomButton>
          </LocationBox>
        ) : null}

        <div>
          <FormControl sx={{ m: 1, minWidth: 150 }}>
            <Select id="type-select" value={type} onChange={handleTypeChange}>
              <MenuItem value={1}>All</MenuItem>
              <MenuItem value={2}>Repair only</MenuItem>
              <MenuItem value={3}>Moving only</MenuItem>
              <MenuItem value={4}>Cleaning only</MenuItem>
              <MenuItem value={5}>Avaiable only</MenuItem>
            </Select>
          </FormControl>
        </div>

        <div>
          <FormControl sx={{ m: 1, minWidth: 150 }}>
            <Select id="sort-select" value={sortType} onChange={handleSortTypeChange}>
              <MenuItem value={1}>Best match</MenuItem>
              <MenuItem value={2}>Date: New - Old</MenuItem>
              <MenuItem value={3}>Date: Old - New</MenuItem>
              <MenuItem value={4}>Price: High - Low</MenuItem>
              <MenuItem value={5}>Price: Low - High</MenuItem>
            </Select>
          </FormControl>
        </div>
      </ToolBar>

      <Contents>
        <TaskCard />
        <TaskCard />
        <TaskCard />
        <TaskCard />
        <TaskCard />
        <TaskCard />
        <TaskCard />
        <TaskCard />
        <TaskCard />
        <TaskCard />
        <TaskCard />
        <TaskCard />
      </Contents>
    </div>
  );
};

export default BrowsePage;
