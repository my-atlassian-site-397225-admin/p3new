import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@mui/material/Avatar';
import DefaultAvator from '../../assets/defaultAvatar.jpg';
import { Card, Section, User, Message } from './CommentBox.style';

const CommentBox = ({ id, message, avatar, username, timeStamp }) => (
  <Card id={id}>
    <Section>
      <User>
        <Avatar alt={username} src={avatar ? avatar : DefaultAvator} />
        <span>{username}</span>
      </User>
      <span>{timeStamp}</span>
    </Section>

    <Message>{message}</Message>
  </Card>
);

CommentBox.propTypes = {
  id: PropTypes.number.isRequired,
  message: PropTypes.string.isRequired,
  avatar: PropTypes.node,
  username: PropTypes.string.isRequired,
  timeStamp: PropTypes.string.isRequired,
};

export default CommentBox;
