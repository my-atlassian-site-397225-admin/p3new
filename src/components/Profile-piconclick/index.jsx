import React from 'react';
import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';
import pic from '../../assets/defaultAvatar.jpg';

const ImageAvatars = () => (
  <Stack direction="row" spacing={2}>
    <Avatar alt="pic" src={pic} onClick={() => alert('avatar')} />
  </Stack>
);

export default ImageAvatars;
