import * as React from 'react';
import Box from '@mui/material/Box';
import { Card, CardContent } from '@mui/material';
import Typography from '@mui/material/Typography';
import { TextStyle, BoxStyle1, BoxStyle2 } from './Text.styled';
// import { CustomizedSnackbars } from './Bar.styled';
import DashboardNotification from '../DashboardNotification';

function Dashboad() {
  return (
    <Box sx={{ width: '90%' }}>
      <Box sx={{ mt: '40px' }}>
        <TextStyle> As a Client </TextStyle>
        <Card style={{ flex: 1, backgroundColor: '#52ab98' }} sx={{ maxWidth: 300, mx: '40px' }}>
          <CardContent>
            <Typography variant="h3" component="div">
              4
            </Typography>
            <Typography variant="h6" component="div">
              Upcoming tasks
            </Typography>
          </CardContent>
        </Card>
      </Box>
      <Box>
        <BoxStyle1 />
        <TextStyle>As a Tasker</TextStyle>
        <Card style={{ margflex: 5, backgroundColor: '#2b6777' }} sx={{ maxWidth: 300, mx: '40px' }}>
          <CardContent>
            <Typography variant="h3" component="div">
              2
            </Typography>
            <Typography variant="h6" component="div">
              Upcoming tasks
            </Typography>
          </CardContent>
        </Card>
      </Box>

      <Box>
        <BoxStyle2>
          <DashboardNotification>Home Clearner has received new offerts</DashboardNotification>
        </BoxStyle2>
      </Box>
    </Box>
  );
}
export default Dashboad;
