import React from 'react';
import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import CustomButton from '../../../CustomButton';

const NotTaskerMode = ({ link }) => {
  return (
    <div>
      <Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            width: '95%',
            justifyContent: 'right',
          }}
        >
          <Link underline="none" href={`/${link}`}>
            <CustomButton color="primary" variant="outlined">
              Turn On Tasker Mode
            </CustomButton>
          </Link>
        </Box>
        <Box
          sx={{
            mt: '200px',
            display: 'flex',
            flexDirection: 'row',
            width: '95%',
            justifyContent: 'center',
            color: '#444',
          }}
        >
          <p> You have turn off the Tasker mode!</p>
        </Box>
      </Box>
    </div>
  );
};
export default NotTaskerMode;
