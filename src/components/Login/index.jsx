import React, { useRef, useEffect, useCallback, useState } from 'react';
import { useSpring, animated } from 'react-spring';
import PropTypes from 'prop-types';
import TextField from '@mui/material/TextField';
import RegisterModal from '../Register/index.jsx';
import CustomButton from '../CustomButton';
import {
  Wrapper,
  Form,
  Background,
  ModalWrapper,
  ModalContent,
  CloseModalButton,
  Text,
  Container,
} from './Login.style.jsx';

const LoginModal = ({ showModal, setShowModal }) => {
  const modalRef = useRef();
  const [showRegisterModal, setShowRegisterModal] = useState(false);

  const [userInfo, setUserInfo] = useState({
    email: '',
    password: '',
  });

  const openRegisterModal = () => {
    setShowModal(false);
    setShowRegisterModal((prev) => !prev);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    //TODO: getPassword from backend and check if its matched
    /*if (userInfo.password !== getPassword) {
      alert("password or username please try again")
    } else {*/
    //TODO: get the id and the picture from backend, change this part later
    localStorage.setItem('userID', '1');
    //localStorage.setItem('avatar',getAvatar)
    //}
  };

  const handleChange = (e) => {
    e.preventDefault();
    setUserInfo({ [e.target.name]: e.target.value });
  };

  const animation = useSpring({
    config: {
      duration: 250,
    },
    opacity: showModal ? 1 : 0,
    transform: showModal ? `translateY(0%)` : `translateY(-100%)`,
  });

  const closeModal = (e) => {
    if (modalRef.current === e.target) {
      setShowModal(false);
    }
  };

  const keyPress = useCallback(
    (e) => {
      if (e.key === 'Escape' && showModal) {
        setShowModal(false);
      }
    },
    [setShowModal, showModal]
  );

  useEffect(() => {
    document.addEventListener('keydown', keyPress);
    return () => document.removeEventListener('keydown', keyPress);
  }, [keyPress]);
  //TODO: maybe add use google, facebook to login
  return (
    <>
      {showModal ? (
        <Background onClick={closeModal} ref={modalRef}>
          <animated.div style={animation}>
            <ModalWrapper showModal={showModal}>
              <ModalContent>
                <Wrapper>
                  <Form onSubmit={handleSubmit}>
                    <TextField
                      type="email"
                      name="email"
                      placeholder="Type your email here..."
                      value={userInfo.email}
                      onChange={handleChange}
                    />
                    <br />
                    <TextField
                      type="password"
                      name="password"
                      placeholder="Enter your password..."
                      value={userInfo.password}
                      onChange={handleChange}
                    />
                    <br />
                    <CustomButton variant="contained" size="large" type="submit">
                      Login
                    </CustomButton>
                    <br />
                    <Text>
                      Forget your password click
                      <CustomButton color="black" size="small">
                        Here
                      </CustomButton>
                    </Text>
                    <Text>
                      Don&apos;t have an account click here
                      <CustomButton color="black" size="small" onClick={openRegisterModal}>
                        Here
                      </CustomButton>
                    </Text>
                  </Form>
                </Wrapper>
              </ModalContent>
              <CloseModalButton aria-label="Close modal" onClick={() => setShowModal((prev) => !prev)} />
            </ModalWrapper>
          </animated.div>
        </Background>
      ) : null}
      <Container>
        <RegisterModal showModal={showRegisterModal} setShowModal={setShowRegisterModal} />
      </Container>
    </>
  );
};

LoginModal.propTypes = {
  showModal: PropTypes.bool,
  setShowModal: PropTypes.func,
};

export default LoginModal;
